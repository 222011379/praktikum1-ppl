<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Products extends Seeder
{
    public function run()
    {
        $products_data = [
            [
                'product_name' => 'Coca Cola',
                'product_price' => 5000,
            ],
            [
                'product_name' => 'Teh Botol',
                'product_price' => 3700,
            ],
            [
                'product_name' => 'You C 1000',
                'product_price' => 6300,
            ],
            [
                'product_name' => 'Ponds Men',
                'product_price' => 18000,
            ],
            [
                'product_name' => 'Rexona Men',
                'product_price' => 13000,
            ],
        ];

        foreach ($products_data as $data) {
            // insert semua data ke tabel
            $this->db->table('product')->insert($data);
        }
    }
}