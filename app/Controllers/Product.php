<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ProductModel;

class Product extends BaseController
{
    public function index()
    {
        $products = new ProductModel();
        $data['product'] = $products->findAll();
        return view('product_view', $data);
    }

    public function create()
    {
        return view('add_product');
    }

    public function store()
    {
        $products = new ProductModel();
        $products->save([
            'product_name' => $this->request->getVar('product_name'),
            'product_price' => $this->request->getVar('product_price'),
        ]);
        return redirect()->to('/product');
    }

    public function delete($id)
    {
        $products = new ProductModel();
        $products->delete($id);
        return redirect()->to('/product');
    }

    public function edit($id)
    {
        $products = new ProductModel();

        $data['product'] = $products->find($id);
        return view('edit_product', $data);
    }

    public function update()
    {
        $products = new ProductModel();
        $products->update($this->request->getVar('product_id'), [
            'product_name' => $this->request->getVar('product_name'),
            'product_price' => $this->request->getVar('product_price'),
        ]);
        return redirect()->to('/product');
    }
}