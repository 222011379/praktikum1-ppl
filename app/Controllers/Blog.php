<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Blog extends BaseController
{
    public function index()
    {
        $data['title'] = 'This Is Title';
        $data['content'] = 'This Is Content';
        return view('blog_view', $data);
    }
}